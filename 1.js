function SpeakerES5(nameInput) {
    if (!(this instanceof SpeakerES5))
        return new SpeakerES5();
    if (arguments.length != 1)
        throw 'Error!';
    this.name = nameInput;
}

SpeakerES5.prototype.speak = function (text) {
    let res = `${this.name} says ${text}`;
    return console.log(res);
};

function ScreamerES5(nameInput) {
    if (!(this instanceof ScreamerES5))
        return new ScreamerES5();
    if (arguments.length != 1)
        throw 'Error!';
    this.name = nameInput;
}

SpeakerES5.__proto__ = ScreamerES5;

ScreamerES5.prototype.speak = function (text) {
    let res = `${this.name} shouts ${text.toUpperCase()}`;
    return console.log(res);
}

var speaker = new SpeakerES5('new_speaker');
speaker.speak('test');
console.dir(speaker);

var screamer = new ScreamerES5('new_screamer');
screamer.speak('test');
console.dir(screamer);